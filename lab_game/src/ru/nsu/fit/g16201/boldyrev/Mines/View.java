package ru.nsu.fit.g16201.boldyrev.Mines;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class View extends JFrame {
    Model model = new Model();
    final String TITLE_OF_PROGRAM = "Mines";
    final String SIGN_OF_FLAG = "f";
    final int BLOCK_SIZE = model.getBLOCK_SIZE(); //in pixels
    final int FIELD_SIZE = model.getFIELD_SIZE(); //in blocks
    final int FIELD_DX = model.getFIELD_DX();
    final int FIELD_DY = model.getFIELD_DY();
    final int START_LOCATION = model.getSTART_LOCATION();
    final int MOUSE_BUTTON_LEFT = model.getMOUSE_BUTTON_LEFT();
    final int MOUSE_BUTTON_RIGHT = model.getMOUSE_BUTTON_RIGHT();
    final int NUMBER_OF_MINES = model.getNUMBER_OF_MINES();
    final int[] COLOR_OF_NUMBERS = {0x0000FF, 0x008000, 0xFF0000, 0x800000, 0x0};
    Model.Cell[][] field = model.getField();
    Random random = new Random();
    int countOpenedCells;
    boolean youWon, bangMine;
    int bangX, bangY;


    View() {
        setTitle("Mines");
        setDefaultCloseOperation(EXIT_ON_CLOSE); //стандартное действие при закрытии приложения через крестик - оно закрывается
        setBounds(START_LOCATION, START_LOCATION, FIELD_SIZE * BLOCK_SIZE + FIELD_DX, FIELD_SIZE * BLOCK_SIZE + FIELD_DY); //отрисовка границ
        setResizable(false);
        //TimerLabel timeLabel = new TimerLabel();
        //timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        Canvas canvas = new Canvas();
        canvas.setBackground(Color.white);
        canvas.addMouseListener(new MouseAdapter() { //В этом теле - описание экземпляра класса MouseAdapter
            @Override
            public void mouseReleased(MouseEvent e) { //mouseReleased - клавиша мыши уже отжата
                super.mouseReleased(e); //вызов метода у родительского класса
                int x = e.getX() / BLOCK_SIZE; //в какую ячейку мы попали
                int y = e.getY() / BLOCK_SIZE;
                if (e.getButton() == MOUSE_BUTTON_LEFT && !bangMine && !youWon) {
                    if (field[y][x].isNotOpen()) {
                        model.openCells(x, y);
                        youWon = countOpenedCells == FIELD_SIZE * FIELD_SIZE - NUMBER_OF_MINES; //определяем, победили ли мы уже
                        if (bangMine) {
                            bangX = x;
                            bangY = y;
                        }
                    }
                }
                if (e.getButton() == MOUSE_BUTTON_RIGHT) {
                    field[y][x].inverseFlag();
                }
                if (bangMine || youWon) {
                    //timeLabel.stopTimer();
                }
                canvas.repaint();
            }
        });
        add(BorderLayout.CENTER, canvas);
        //add(BorderLayout.SOUTH, timeLabel);
        setVisible(true);
        model.initField();
    }

    

    class Canvas extends JPanel {
        @Override
        public void paint(Graphics g) {
            super.paint(g);
            for (int x = 0; x < FIELD_SIZE; x++) {
                for (int y = 0; y < FIELD_SIZE; y++) {
                    field[y][x].paint(g, x, y);
                }
            }
        }
    }
}
