package ru.nsu.fit.g16201.boldyrev.Mines;

import javax.swing.*;
import java.awt.*;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Model {
    final String TITLE_OF_PROGRAM = "Mines";
    final String SIGN_OF_FLAG = "f";
    final int BLOCK_SIZE = 30; //in pixels
    final int FIELD_SIZE = 9; //in blocks
    final int FIELD_DX = 0;
    final int FIELD_DY = 53;
    final int START_LOCATION = 200;
    final int MOUSE_BUTTON_LEFT = 1;
    final int MOUSE_BUTTON_RIGHT = 3;
    final int NUMBER_OF_MINES = 10;
    final int[] COLOR_OF_NUMBERS = {0x0000FF, 0x008000, 0xFF0000, 0x800000, 0x0};
    Cell[][] field = new Cell[FIELD_SIZE][FIELD_SIZE];
    Random random = new Random();
    int countOpenedCells;
    boolean youWon, bangMine;
    int bangX, bangY;

    class Cell {
        private boolean isOpen, isMine, isFlag;
        private int countBombNear = 0;

        void open() {
            isOpen = true;
            bangMine = isMine;
            if (!isMine) {
                countOpenedCells++;
            }
        }

        boolean isNotOpen() {
            return !isOpen;
        }

        void inverseFlag() {
            isFlag = !isFlag;
        }

        boolean isMined() {
            return isMine;
        }

        void mine() {
            isMine = true;
        }

        void setCountBomb(int count) {
            countBombNear = count;
        }

        int getCountBomb() {
            return countBombNear;
        }

        void paint(Graphics g, int x, int y) {
            g.setColor(Color.lightGray);
            g.drawRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
            if (!isOpen) {
                if ((bangMine || youWon) && isMine) {
                    paintBomb(g, x, y, Color.black);
                } else {
                    g.setColor(Color.lightGray);
                    g.fill3DRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, true);
                    if (isFlag) {
                        paintString(g, SIGN_OF_FLAG, x, y, Color.red);
                    }
                }
            } else {
                if (isMine) {
                    paintBomb(g,  x, y, bangMine? Color.red : Color.black);
                }
                else {
                    if (countBombNear > 0) {
                        paintString(g, Integer.toString(countBombNear), x, y, new Color(COLOR_OF_NUMBERS[countBombNear - 1]));
                    }
                }
            }
        }

        void paintBomb(Graphics g, int x, int y, Color color) {
            g.setColor(color);
            g.fillRect(x * BLOCK_SIZE + 7, y * BLOCK_SIZE + 10, 18, 10);
            g.fillRect(x * BLOCK_SIZE + 11, y * BLOCK_SIZE + 6, 10, 18);
            g.fillRect(x * BLOCK_SIZE + 9, y * BLOCK_SIZE + 8, 14, 14);
            g.setColor(Color.white);
            g.fillRect(x * BLOCK_SIZE + 11, y * BLOCK_SIZE + 10, 4, 4);
        }

        void paintString(Graphics g, String str, int x, int y, Color color) {
            g.setColor(color);
            g.setFont(new Font("", Font.BOLD, BLOCK_SIZE));
            g.drawString(str, x * BLOCK_SIZE + 8, y * BLOCK_SIZE + 26);
        }
    }

    void openCells(int x, int y) {class Canvas extends JPanel {
        @Override
        public void paint(Graphics g) {
            super.paint(g);
            for (int x = 0; x < FIELD_SIZE; x++) {
                for (int y = 0; y < FIELD_SIZE; y++) {
                    field[y][x].paint(g, x, y);
                }
            }
        }
    }
        if (x < 0 || x > FIELD_SIZE - 1 || y < 0 || y > FIELD_SIZE - 1) {
            return;
        }
        if (!field[y][x].isNotOpen()) {
            return;
        }
        field[y][x].open();
        if (field[y][x].getCountBomb() > 0 || bangMine) { //условие bangMine - иначе некоторые клетки с бомбами будут открываться как обычные
            return;
        }
        for (int dx = -1; dx < 2; dx++) {
            for (int dy = 0; dy < 2; dy++) {
                openCells(x + dx, y + dy);
            }
        }
    }

    void initField() { //инициализация игрового поля
        int x = 0;
        int y = 0;
        int countMines = 0;

        //создаём ячейки поля
        for (x = 0; x < FIELD_SIZE; x++) {
            for (y = 0; y < FIELD_SIZE; y++) {
                field[y][x] = new Cell();
            }
        }

        //рандомно расставляем мины
        while(countMines < NUMBER_OF_MINES) {
            do {
                x = random.nextInt(FIELD_SIZE);
                y = random.nextInt(FIELD_SIZE);
            } while (field[y][x].isMined());
            field[y][x].mine();
            countMines++;
        }

        //подсчёт заминированных вокруг ячейки соседей
        for (x = 0; x < FIELD_SIZE; x++) {
            for (y = 0; y < FIELD_SIZE; y++) {
                if (!field[y][x].isMined()) {
                    int count = 0;
                    for (int dx = -1; dx < 2; dx++) {
                        for (int dy = -1; dy < 2; dy++) {
                            int nX = x + dx;
                            int nY = y + dy;
                            if (nX < 0 || nY < 0 || nX > FIELD_SIZE - 1 || nY > FIELD_SIZE - 1) {
                                nX = x;
                                nY = y;
                            }
                            count += (field[nY][nX].isMined()) ? 1 : 0;
                        }
                    }
                    field[y][x].setCountBomb(count);
                }
            }
        }
    }

    class TimerLabel extends JLabel {
        java.util.Timer timer = new Timer();

        TimerLabel() {
            timer.scheduleAtFixedRate(timerTask, 0, 1000);
        }

        TimerTask timerTask = new TimerTask() {
            volatile int time;
            Runnable refresher = new Runnable() {
                public void run() {
                    TimerLabel.this.setText(String.format("%02d:%02d", time / 60, time % 60));
                }
            };
            public void run() {
                time++;
                SwingUtilities.invokeLater(refresher);
            }
        };

        void stopTimer() {
            timer.cancel();
        }
    }

    public int getBLOCK_SIZE() {
        return BLOCK_SIZE;
    }

    public int getFIELD_SIZE() {
        return FIELD_SIZE;
    }

    public int getFIELD_DX() {  return FIELD_DX; }

    public int getFIELD_DY() {
        return FIELD_DY;
    }

    public int getSTART_LOCATION() {
        return START_LOCATION;
    }

    public int getMOUSE_BUTTON_LEFT() { return MOUSE_BUTTON_LEFT; }

    public int getMOUSE_BUTTON_RIGHT() {
        return MOUSE_BUTTON_RIGHT;
    }

    public int getNUMBER_OF_MINES() {
        return NUMBER_OF_MINES;
    }

    public int getCountOpenedCells() {
        return countOpenedCells;
    }

    public int getBangX() {
        return bangX;
    }

    public int getBangY() {
        return bangY;
    }

    boolean getYouWon() { return youWon; }

    boolean getBangMine() { return bangMine; }

    Cell[][] getField() { return field; }
}
